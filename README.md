# health-punch-web

> 疫情打卡（VUE+Element+Baidu地图 前端工程）
# 视频演示
https://www.bilibili.com/video/BV1Vw411Z76D?share_source=copy_web
### 登录以及注册页
![img_1.png](img_1.png)
![img_2.png](img_2.png)
### 主页面（打卡）
![img.png](img.png)
### 记录页
> 有管理员和普通用户数据展示
![img_3.png](img_3.png)
### 修改密码
![img_4.png](img_4.png)

##  :bangbang: node 版本需要12.0以上

## 项目启动
``` bash
# 第一步
npm install

# 第二步（开发环境下运行）
npm run dev

# 第三步
（前提）跑起后端，然后就可以耍了!!!

# 打包 （生产环境下运行）
npm run build
```
#### 最后

 **记得给我一个星星喔，星星是我开源的动力** 

##### 有技术上的问题欢迎进群讨论（群里定期发放红包福利）


点击链接加入群聊【LIEFox技术交流群】：https://jq.qq.com/?_wv=1027&k=XKRmblB9


## 个人官方网站 https://cxq21.gitee.io/